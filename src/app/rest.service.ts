import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class RestService {
  url: string = "http://localhost:3000/data/";
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(this.url);
  }
}
