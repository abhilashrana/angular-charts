    export class CallRecordingDisclosureData {
        label: string;
        y: number;
    }

    export class CallOpeningComplianceData {
        label: string;
        y: number;
    }

    export class EmpathyOrSympathyData {
        label: string;
        y: number;
    }

    export class OtherLobCheckData {
        label: string;
        y: number;
    }

    export class CancelTechVisitData {
        label: string;
        y: number;
    }

    export class SelfHelpXfinityMyAccountAppData {
        label: string;
        y: number;
    }

    export class CallClosingData {
        label: string;
        y: number;
    }

    export class BehaviouralSentimentData {
        behaviour: string;
        y: number;
    }

    export class Datum {
        call_recording_disclosure_data: CallRecordingDisclosureData[];
        call_opening_compliance_data: CallOpeningComplianceData[];
        empathy_or_sympathy_data: EmpathyOrSympathyData[];
        other_lob_check_data: OtherLobCheckData[];
        cancel_tech_visit_data: CancelTechVisitData[];
        self_help_xfinity_my_account_app_data: SelfHelpXfinityMyAccountAppData[];
        call_closing_data: CallClosingData[];
        behavioural_sentiment_data: BehaviouralSentimentData[];
    }

    export class DataObject {
        countries: string[];
        data: Datum[];
    }

