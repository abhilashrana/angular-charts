import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../assets/canvasjs.min';
import { RestService } from './rest.service';
import { DataObject } from './model';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  dataList : DataObject;
  constructor(private rs: RestService) {}

  ngOnInit() {
	  this.getData();    
  }

  getData() {
    this.rs.getUsers().subscribe(
      (response: DataObject) => {
		  this.dataList = response;
		  let chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			exportEnabled: true,
			title: {
			  text: "Basic Column Chart in Angular",
			},
			data: [
			  {
				type: "column",
				dataPoints: this.dataList[0].call_recording_disclosure_data
			  },
			],
		  });	  
		  chart.render();
		  let chart2 = new CanvasJS.Chart("chartContainer2", {
			animationEnabled: true,
			exportEnabled: true,
			title: {
			  text: "Basic Line Chart in Angular",
			},
			data: [
			  {
				type: "line",
				dataPoints: this.dataList[1].call_opening_compliance_data
			  },
			],
		  });	  
		  chart2.render();
		  let chart3 = new CanvasJS.Chart("chartContainer3", {
			animationEnabled: true,
			exportEnabled: true,
			title: {
			  text: "Basic Bar Chart in Angular",
			},
			data: [
			  {
				type: "bar",
				dataPoints: this.dataList[2].empathy_or_sympathy_data
			  },
			],
		  });	  
		  chart3.render();
		  let chart4 = new CanvasJS.Chart("chartContainer4", {
			animationEnabled: true,
			exportEnabled: true,
			title: {
			  text: "Basic pie Chart in Angular",
			},
			data: [
			  {
				type: "pie",
				dataPoints: this.dataList[3].other_lob_check_data
			  },
			],
		  });	  
		  chart4.render();

		  let chart5 = new CanvasJS.Chart("chartContainer5", {
			animationEnabled: true,
			exportEnabled: true,
			title: {
			  text: "Basic spline Chart in Angular",
			},
			data: [
			  {
				type: "spline",
				dataPoints: this.dataList[4].cancel_tech_visit_data
			  },
			],
		  });	  
		  chart5.render();
      },
      (error) => console.log(error)
    );
  }

  
}


